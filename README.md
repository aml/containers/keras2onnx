Keras to Onnx (and lwtnn) converter
===================================

This is a docker image to convert keras models to both onnx and lwtnn
format. It will recursively step through a directory you mount and try
to convert anything that ends in `.h5`.


Quick Start
===========

Go to a directory where your files live, and run this command.

```
docker run -v $PWD:/convert --rm keras2onnx
```

or, if you are on a machine that only has singularity:

```
singularity run -B $PWD:/convert docker://gitlab-registry.cern.ch/aml/containers/keras2onnx/keras2onnx
```

or if you have singularity and cvmfs:

```
singularity run -B $PWD:/convert '/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/aml/containers/keras2onnx/keras2onnx:latest'
```

Note that the `.h5` models should be whatever comes out of keras when
you run `model.save('path.h5')`.

It's important that you mount whatever directory you want to convert
to `/work`, but the `$PWD` can be changed to any path: the container
will run over whatever is there and try to convert.


Making dummy networks
=====================

You can also use this to generate simple dummy networks. Run

```
docker run -v $PWD:/convert --rm keras2onnx make-network --help
```

for more options.


Thanks
======

Thanks to Matthew Feickert for his [example images][1], most things
are copied from there.

[1]: https://gitlab.cern.ch/aml/containers/docker
