FROM python:3.7-slim

RUN \
  apt-get update && \
  apt-get install -y wget && \
  pip3 install --upgrade pip && \
  pip3 install tensorflow==2.2 keras2onnx && \
  true

RUN \
  mkdir lwtnn && ( \
  cd lwtnn && \
  wget -q https://github.com/lwtnn/lwtnn/archive/master.tar.gz && \
  tar xf master.tar.gz &&  \
  cp lwtnn-*/converters/* lwtnn-*/scripts/* /bin/ ) && \
  rm -rf lwtnn && \
  true

COPY scripts/convert-keras-to-onnx.py /bin/
COPY scripts/convert-all-models.sh /bin/
COPY scripts/make-network /bin/

RUN hash

WORKDIR /convert

CMD ["convert-all-models.sh"]
