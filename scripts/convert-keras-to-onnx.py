#!/usr/bin/env python3

"""
Convert Keras to Onnx converter
"""

import sys
import argparse
import keras2onnx as oml
from pathlib import Path

def convert_to_onnx(keras_model, save_path):
    onnx_model = oml.convert_keras(keras_model)
    oml.save_model(onnx_model, str(save_path))

def get_args():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('model', type=Path)
    return parser.parse_args()

def run():
    args = get_args()
    from tensorflow.keras.models import load_model
    model = load_model(args.model)
    output = args.model.with_suffix('.onnx')
    convert_to_onnx(model, output)

if __name__ == '__main__':
    run()
