#!/usr/bin/env bash

set -eu

function make-lwtnn () {
    tmpdir=$(mktemp -d)
    cp $1 $tmpdir
    kout=$(basename $1)
    lout=${kout%.h5}.json
    (
        cd $tmpdir
        if ! lwtnn-split-keras-network.py -a ar.json -w w.h5 $kout &> /dev/null
        then
            return 1
        fi
        if ! kerasfunc2json.py ar.json w.h5 > vars.json 2> log.txt
        then
            echo "$1 might not be graph model" >&2
            cat log.txt >&2
            return 1
        fi
        kerasfunc2json.py ar.json w.h5 vars.json > $lout 2> /dev/null
    )
    if [[ -f $tmpdir/$lout ]]
    then
        cp $tmpdir/$lout ${1%.h5}.json
    else
        echo "can't convert $1 to lwtnn" >&2
        return 1
    fi
}

for model in $(find ${@-.} -type f -name '*.h5' ); do
    if make-lwtnn $model
    then
        convert-keras-to-onnx.py $model &> /dev/null
        if ! [[ -f ${model%.h5}.onnx ]]
        then
            echo "can't convert ${model} to onnx" >&2
        fi
    fi
done
